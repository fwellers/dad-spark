package org.atons;

import org.apache.kafka.common.security.plain.PlainLoginModule;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQueryException;

import java.util.concurrent.TimeoutException;


public class Main {
    public static void main(String[] args) {
        String KAFKA_URL = System.getenv("KAFKA_SERVICE_HOST") + ":" + System.getenv("KAFKA_SERVICE_PORT");
        String KAFKA_CLIENT_USERS = System.getenv("user1");
        String KAFKA_CLIENT_PASSWORDS = System.getenv("NqwY5YnO6s");
        if (KAFKA_URL == null || KAFKA_URL.isEmpty()) {
            KAFKA_URL = "localhost:9092";
        }
        System.out.println("Using kafka url " + KAFKA_URL);

        SparkSession spark = SparkSession.builder()
                .appName("JavaArticleCount")
                .getOrCreate();

        String TOPIC = "rss";
        Dataset<Row> dataset = spark.readStream()
                .format("kafka")
                .option("kafka.bootstrap.servers", KAFKA_URL)
                .option("subscribe", TOPIC)
                .option("kafka.security.protocol", "SASL_PLAINTEXT")
                .option("kafka.sasl.mechanism", "PLAIN")
                .option("kafka.sasl.jaas.config", PlainLoginModule.class.getName() + " required username=\"" + KAFKA_CLIENT_USERS + "\" password=\"" + KAFKA_CLIENT_PASSWORDS + "\";")
                .load()
                .selectExpr("CAST(value as STRING)"); //theoretisch um binarytype in string zu konvertieren


        //MVP Beispiel 
        dataset = dataset.withColumn("timestamp", functions.current_timestamp()); //add new column "timestamp" mit timestamp

        //write to console
        try {
            dataset.writeStream()
                    .format("console")
                    .outputMode("append")
                    .start()
                    .awaitTermination();

            dataset.writeStream()
                    .format("kafka")
                    .outputMode("append")
                    .option("topic", "results-test")
                    .option("kafka.bootstrap.servers", KAFKA_URL)
                    .start()
                    .awaitTermination();
        } catch (StreamingQueryException e) {
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }

        //write to kafka, topic: results-test


        //Ideen für Analyse
        //für alle Newspaper, über die Theman Corona, Ukraine und Israel/Palästina gehen und suchen, ob im Titel relevante Stichwörter sind
        //dann jeweils Mengen zählen

        //unklar: mergen der Ergebnisse auf die gesamten Daten 
/* 
        ArrayList<String> newspaper = new ArrayList<String>(); 
        newspaper.add("taz"); 
        newspaper.add("welt"); 
        newspaper.add("nytimes"); 
        //append list 

        //words indicating a title regards to a topic
        ArrayList<String> ukraineTags = new ArrayList<String>(); 
        newspaper.add("ukraine"); 
        newspaper.add("Ukraine"); 
        newspaper.add("russia"); 
        newspaper.add("Russland"); 

        ArrayList<String> israelTags = new ArrayList<String>(); 
        newspaper.add("israel"); 
        newspaper.add("Israel"); 
        newspaper.add("palestine"); 
        newspaper.add("Palästina"); 

        ArrayList<String> covidTags = new ArrayList<String>(); 
        newspaper.add("covid"); 
        newspaper.add("Covid"); 
        newspaper.add("corona"); 
        newspaper.add("Corona"); 

        ArrayList<ArrayList<String>> relevantTopics = new ArrayList<ArrayList<String>>(); 
        relevantTopics.add(ukraineTags); 
        relevantTopics.add(israelTags); 
        relevantTopics.add(covidTags); 

              
        //count appearances of different topic per newspaper
        //nun über den aktuellen Datensatz gehen und für jeden Newspapertype (substring in link) nach Inhalt (substring im title) suchen und aggregieren
        for (String newschannel: newspaper){
            
            for (ArrayList<String> topicList: relevantTopics)
            {
                String query = "SELECT COUNT(*) as " + topicList.get(0) + "count"; 
                for(String token: topicList)
                {
                    String queryExtension = "WHERE title LIKE " + token + " OR "; 
                    query = query + queryExtension; 
                }
                query = query.substring(0, query.length() - 4); //remove last " OR " 
                query = query + " AND link LIKE " + "'" + newschannel + "'";     
            }
        }

        //TODO unterteilen in ID und eigenschaften
        StructType resultSchema = new StructType()
            .add("newspaper", StringType)
            .add("totalEntries", IntegerType)
            .add("ukraineEntries", IntegerType)
            .add("israelEntries", IntegerType)
            .add("covidEntries", IntegerType); 
        //TODO write results into kafka readable format - hopefully like this schema? 
        //TODO send to kafka
     */

            
     /*    //
        try {
            tazEntries.writeStream()
                .format("kafka")
                .outputMode("append")
                .option("topic", "results")
                .start()
                .awaitTermination();
        } catch (StreamingQueryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TimeoutException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } */

    }
}