FROM openjdk:17
WORKDIR spark
ADD build/libs/dad-spark-1.0-SNAPSHOT.jar dad-spark-1.0-SNAPSHOT.jar
CMD ["java","-jar","dad-spark-1.0-SNAPSHOT.jar"]